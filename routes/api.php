<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use \App\Http\Controllers\Ia14Controller;
use \App\Http\Controllers\Ia7Controller;
use \App\Http\Controllers\CasosController;
use \App\Http\Controllers\MuertosController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::resource('Ia14', Ia14Controller::class);
Route::get('ia14/{fecha}', [Ia14Controller::class, 'show']);
Route::get('ia14all', [Ia14Controller::class, 'showAll']);
Route::post('ia14add', [Ia14Controller::class, 'store']);
Route::put('ia14Update', [Ia14Controller::class, 'update']); //No funciona
Route::put('ia14Delete/{fecha}', [Ia14Controller::class, 'destroy']); //No funciona

Route::resource('Ia7', Ia7Controller::class);
Route::get('ia7/{fecha}', [Ia7Controller::class, 'show']);
Route::get('ia7all', [Ia7Controller::class, 'showAll']);
Route::post('ia7add', [Ia7Controller::class, 'store']);

Route::resource('Casos', CasosController::class);
Route::get('casos/{fecha}', [CasosController::class, 'show']);

Route::resource('Muertos', MuertosController::class);
Route::get('muertos/{fecha}', [MuertosController::class, 'show']);

// Mostrar colecciones
Route::get('ia14/{fecha}/{fecha2}', [Ia14Controller::class, 'showCollection']);
Route::get('ia7/{fecha}/{fecha2}', [Ia7Controller::class, 'showCollection']);
Route::get('casos/{fecha}/{fecha2}', [CasosController::class, 'showCollection']);
Route::get('muertos/{fecha}/{fecha2}', [MuertosController::class, 'showCollection']);
