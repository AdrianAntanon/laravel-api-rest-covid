<?php

namespace App\Http\Controllers;

use App\Http\Resources\CovidCollection;
use App\Models\ia7;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class Ia7Controller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $ia7 = new ia7();

        $ia7 ->fecha = $request->fecha;
        $ia7 ->ccaas_id = $request->ccaas_id;
        $ia7 ->incidencia = $request->incidencia;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $ia7 = DB::select(DB::raw("SELECT * FROM ia7 WHERE fecha='$id'"));
        if (!$ia7){
            return response()->json(['errors'=>Array(['code'=> 404, 'message'=>'No existe la fecha'])], 404);
        }
        return response()->json(['status'=>'ok', 'data'=>$ia7], 200);
    }

    public function showCollection($id, $id2)
    {
        if ($id>$id2){
            return response()->json(['errors'=>Array(['code'=> 404, 'message'=>'Primera fecha superior a la segunda'])], 404);
        }
        $ia7 = DB::select(DB::raw("SELECT * FROM ia7 WHERE fecha BETWEEN '$id' and '$id2'"));
        if (!$ia7){
            return response()->json(['errors'=>Array(['code'=> 404, 'message'=>'No existe la fecha'])], 404);
        }
        return new CovidCollection($ia7);
    }

    public function showAll()
    {
        $ia7 = ia7::all();
        if (!$ia7){
            return response()->json(['errors'=>Array(['code'=> 404, 'message'=>'No existe la fecha'])], 404);
        }
        return response()->json(['status'=>'ok', 'data'=>$ia7], 200);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
