<?php

namespace App\Http\Resources;

use App\Models\CCAAs;

use App\Models\ia14;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\DB;
use function PHPUnit\Framework\isInstanceOf;

class ShowResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $pais = DB::table('paises')
            ->join('ccaas', 'paises.id', '=', 'ccaas.pais_id')
            ->select('paises.*')->first();

        $ccaa = CCAAs::where('id', $this->ccaas_id)->first();
       // dd($this);

        if (isset($this->incidencia)){

            return [
                'fecha'=>$this->fecha,
                'pais'=>$pais->nombre,
                'ccaas'=>$ccaa->nombre,
                'value' => $this->incidencia,
            ];
        }else{
            return [
                'fecha'=>$this->fecha,
                'pais'=>$pais->nombre,
                'ccaas'=>$ccaa->nombre,
                'value' => $this->numero,
            ];
        }
    }
}
